package org.example.app.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.example.app.dto.TicketCreateRQ;
import org.example.app.dto.TicketCreateRS;
import org.example.app.dto.TicketGetAllRS;
import org.example.app.service.TicketService;
import org.example.framework.authentication.Authentication;
import org.springframework.stereotype.Controller;

import java.io.IOException;
import java.util.List;


@Controller
@RequiredArgsConstructor
public class TicketController {
    private final Gson gson;
    private final TicketService service;

    public void getAll(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
        final Authentication authentication = (Authentication) req.getAttribute(Authentication.ATTR_AUTH);
        final List<TicketGetAllRS> tickets = service.getAllTickets(authentication);
        final String response = gson.toJson(tickets);
        resp.getWriter().write(response);
    }

    public void create(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
        final Authentication authentication = (Authentication) req.getAttribute(Authentication.ATTR_AUTH);
        final TicketCreateRQ ticketCreateRQ = gson.fromJson(new JsonReader(req.getReader()), TicketCreateRQ.class);
        final TicketCreateRS ticket = service.createTicket(authentication, ticketCreateRQ);
        final String response = gson.toJson(ticket);
        resp.getWriter().write(response);
    }

    public void getById(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
        final Authentication authentication = (Authentication) req.getAttribute(Authentication.ATTR_AUTH);
        final Long id = Long.parseLong(req.getParameter("id"));
        final TicketCreateRS ticket = service.getTicketById(authentication, id);
        final String response = gson.toJson(ticket);
        resp.getWriter().write(response);
    }

    public void getCloseTicket(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
        final Authentication authentication = (Authentication) req.getAttribute(Authentication.ATTR_AUTH);
        final Long id = Long.parseLong(req.getParameter("id"));
        service.getCloseTicket(authentication, id);
        final String message = "Ticket with id: " + id + " close";
        final String response = gson.toJson(message);
        resp.getWriter().write(response);

    }
}
