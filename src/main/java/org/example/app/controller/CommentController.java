package org.example.app.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonReader;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.example.app.dto.CommentCreateRQ;
import org.example.app.dto.CommentCreateRS;
import org.example.app.dto.TicketCreateRS;
import org.example.app.dto.TicketGetAllRS;
import org.example.app.service.CommentService;
import org.example.framework.authentication.Authentication;
import org.springframework.stereotype.Controller;

import java.io.IOException;
import java.util.List;

@Controller
@RequiredArgsConstructor
public class CommentController {
    private final Gson gson;
    private final CommentService service;

    public void getAll(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
        final Authentication authentication = (Authentication) req.getAttribute(Authentication.ATTR_AUTH);
        final List<TicketGetAllRS> tickets = service.getAllTickets(authentication);
        final String response = gson.toJson(tickets);
        resp.getWriter().write(response);
    }

    public void create(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
        final Authentication authentication = (Authentication) req.getAttribute(Authentication.ATTR_AUTH);
        final Long id = Long.parseLong(req.getParameter("id"));
        final CommentCreateRQ commentCreateRQ = gson.fromJson(new JsonReader(req.getReader()), CommentCreateRQ.class);
        final CommentCreateRS comment = service.createComment(authentication, commentCreateRQ, id);
        final String response = gson.toJson(comment);
        resp.getWriter().write(response);
    }

    public void getById(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
        final Authentication authentication = (Authentication) req.getAttribute(Authentication.ATTR_AUTH);
        final Long id = Long.parseLong(req.getParameter("id"));
        final TicketCreateRS ticket = service.getTicketById(authentication, id);
        final String response = gson.toJson(ticket);
        resp.getWriter().write(response);
    }
}
