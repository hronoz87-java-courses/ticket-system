package org.example.app.controller;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.example.app.service.UserService;
import org.example.framework.authentication.Authentication;
import org.springframework.stereotype.Controller;

import java.io.IOException;

@Controller
@RequiredArgsConstructor
public class UserController {
  private final UserService service;

  public void me(final HttpServletRequest req, final HttpServletResponse resp) throws IOException {
    final Authentication authentication = (Authentication) req.getAttribute(Authentication.ATTR_AUTH);
    resp.getWriter().write("me: " + authentication.getLogin());
  }
}
