package org.example.app.configuration;

import com.google.gson.Gson;
import org.example.app.controller.CommentController;
import org.example.app.controller.TicketController;
import org.example.app.controller.UserController;
import org.example.framework.context.ContextBeanNames;
import org.example.framework.handler.Handler;
import org.example.framework.template.JpaTransactionTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jndi.JndiTemplate;

import javax.naming.NamingException;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration(proxyBeanMethods = false)
public class ApplicationConfiguration {

    @Bean
    public Gson gson() {
        return new Gson();
    }

    @Bean
    public JndiTemplate jndiTemplate() {
        return new JndiTemplate();
    }

    @Bean
    public DataSource dataSource(final JndiTemplate template) throws NamingException {
        return template.lookup("java:/comp/env/jdbc/db", DataSource.class);
    }

    @Bean
    public JpaTransactionTemplate jpaTransactionTemplate() {
        final EntityManagerFactory emf = Persistence.createEntityManagerFactory("default");
        return new JpaTransactionTemplate(emf);
    }

    @Bean(ContextBeanNames.ROUTES)
    public Map<String, Handler> routes(final UserController userController,
                                       final TicketController ticketController,
                                       final CommentController commentController) {

        Map<String, Handler> maps = new HashMap<>();
        maps.put("/user/me", userController::me);
        maps.put("/ticket/create", ticketController::create);
        maps.put("/ticket/getAll", ticketController::getAll);
        maps.put("/ticket/getById", ticketController::getById);
        maps.put("/ticket/close", ticketController::getCloseTicket);
        maps.put("/support/getAll", commentController::getAll);
        maps.put("/support/comment", commentController::create);
        maps.put("/support/getById", commentController::getById);

        return maps;
    }

}
