package org.example.app.service;

import lombok.RequiredArgsConstructor;
import org.example.app.entity.UserEntity;
import org.example.app.repository.UserRepository;
import org.example.framework.authentication.Authentication;
import org.example.framework.authentication.LoginPasswordAuthentication;
import org.example.framework.authenticator.Authenticator;
import org.example.framework.exception.AuthenticationException;
import org.example.framework.exception.UserNotFoundException;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService implements Authenticator {
    private final UserRepository repository;

    @Override
    public Authentication authenticate(String login, String password) throws AuthenticationException {
        final UserEntity user = repository.getByLogin(login);
        if (user != null) {
          return new LoginPasswordAuthentication(user.getLogin(), user.getPassword());
        }
        throw new UserNotFoundException(login);
    }
}
