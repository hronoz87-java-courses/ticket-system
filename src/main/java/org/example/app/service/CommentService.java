package org.example.app.service;

import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.example.app.dto.*;
import org.example.app.entity.CommentEntity;
import org.example.app.entity.TicketEntity;
import org.example.app.entity.UserEntity;
import org.example.app.mapper.TicketMapper;
import org.example.app.mapper.CommentMapper;
import org.example.app.repository.CommentRepository;
import org.example.app.exception.UserNotSupportRoleException;
import org.example.framework.authentication.Authentication;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.example.app.service.NameRole.SUPPORT;

@Service
@RequiredArgsConstructor
public class CommentService {
    private final CommentRepository commentRepository;
    private final CommentMapper commentMapper;
    private final TicketMapper ticketMapper;
    private final Gson gson;
    private static final int SEARCH_SIZE_LIMIT = 50;
    private static final int SEARCH_START = 0;


    public List<TicketGetAllRS> getAllTickets(final Authentication authentication) {
        checkRoleSupport(authentication);
        final List<TicketEntity> tickets = commentRepository.getAllTickets(SEARCH_SIZE_LIMIT, SEARCH_START);
        return commentMapper.toListOfTicketGetByLogin(tickets);
    }

    public CommentCreateRS createComment(final Authentication authentication, final CommentCreateRQ req, final Long id) {
        checkRoleSupport(authentication);
        CommentEntity commentEntity = commentMapper.fromCommentRQ(
                req,
                req.getAuthor(),
                req.getText()
        );
        final CommentEntity comment = commentRepository.createComment(commentEntity, id);
        return commentMapper.toCommentRS(comment);
    }

    public TicketCreateRS getTicketById(final Authentication authentication, final Long id) {
        checkRoleSupport(authentication);
        final TicketEntity ticket = commentRepository.getByTicketId(id);
        final List<CommentEntity> comments = commentRepository.getCommentsByTicketId(id);
        final List<CommentGetAllRS> commentGetAllRS = commentMapper.toListComments(comments);
        final String json = gson.toJson(commentGetAllRS);
        ticket.setComments(json);
        return ticketMapper.toTicketCreateRS(ticket);
    }

    private UserEntity checkRoleSupport(final Authentication authentication) {
        final UserEntity entity = commentRepository.getByLogin(authentication.getLogin());
        final String role = entity.getRole();
        if (role.equals(SUPPORT)) {
            return entity;
        }
        throw new UserNotSupportRoleException(authentication.getLogin());
    }

}
