package org.example.app.service;

import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.example.app.dto.*;
import org.example.app.entity.CommentEntity;
import org.example.app.entity.TicketEntity;
import org.example.app.mapper.CommentMapper;
import org.example.app.mapper.TicketMapper;
import org.example.app.repository.CommentRepository;
import org.example.app.repository.TicketRepository;
import org.example.framework.authentication.Authentication;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TicketService {
    private final TicketRepository ticketRepository;
    private final TicketMapper ticketMapper;
    private final CommentRepository commentRepository;
    private final CommentMapper commentMapper;
    private final Gson gson;
    private static final int SEARCH_SIZE_LIMIT = 50;
    private static final int SEARCH_START = 0;

    public List<TicketGetAllRS> getAllTickets(final Authentication authentication) {
        final List<TicketEntity> tickets = ticketRepository.getAllTicketLogin(authentication, SEARCH_SIZE_LIMIT, SEARCH_START);
        return ticketMapper.toListOfTicketGetByLogin(tickets);
    }

    public TicketCreateRS createTicket(final Authentication authentication, final TicketCreateRQ req) {
        TicketEntity ticketEntity = ticketMapper.fromTicketCreateRQ(
                req,
                req.getName(),
                req.getContent()
        );
        final TicketEntity ticket = ticketRepository.createTicket(authentication, ticketEntity);
        return ticketMapper.toTicketCreateRS(ticket);
    }


    public TicketCreateRS getTicketById(final Authentication authentication, final Long id) {
        final TicketEntity ticket = ticketRepository.getByTicketId(authentication, id);
        final List<CommentEntity> comments = commentRepository.getCommentsByTicketId(id);
        final List<CommentGetAllRS> commentGetAllRS = commentMapper.toListComments(comments);
        final String json = gson.toJson(commentGetAllRS);
        ticket.setComments(json);
        return ticketMapper.toTicketCreateRS(ticket);
    }

    public Long getCloseTicket(final Authentication authentication, final Long id) {
        return ticketRepository.getCloseTicket(authentication, id);

    }
}
