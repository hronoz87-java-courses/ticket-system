package org.example.app.mapper;

import org.example.app.dto.TicketCreateRQ;
import org.example.app.dto.TicketCreateRS;
import org.example.app.dto.TicketGetAllRS;
import org.example.app.entity.TicketEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

import java.util.List;

@Mapper
public interface TicketMapper {
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "comments", ignore = true)
    @Mapping(target = "status", ignore = true)
    @Mapping(target = "userId", ignore = true)
    @Mapping(target = "created", ignore = true)
    TicketEntity fromTicketCreateRQ(
            final TicketCreateRQ req,
            final String name,
            final String content
    );

    TicketCreateRS toTicketCreateRS(final TicketEntity ticket);

    @Mapping(target = "comment", ignore = true)
    List<TicketGetAllRS> toListOfTicketGetByLogin(final List<TicketEntity> tickets);
}
