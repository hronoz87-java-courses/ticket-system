package org.example.app.exception;

public class UserNotSupportRoleException extends ApplicationException {

    public UserNotSupportRoleException() {
    }

    public UserNotSupportRoleException(String message) {
        super(message);
    }

    public UserNotSupportRoleException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserNotSupportRoleException(Throwable cause) {
        super(cause);
    }

    public UserNotSupportRoleException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
