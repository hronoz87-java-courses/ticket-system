package org.example.app.repository;

import lombok.RequiredArgsConstructor;
import org.example.app.entity.UserEntity;
import org.example.framework.template.JpaTransactionTemplate;
import org.springframework.stereotype.Repository;

@Repository
@RequiredArgsConstructor
public class UserRepository {
    private final JpaTransactionTemplate template;

    public UserEntity getByLogin(final String login) {
        return template.executeInTransaction(em ->
                em.createQuery("SELECT e FROM UserEntity e WHERE e.login = :login", UserEntity.class)
                        .setParameter("login", login.toLowerCase()).getSingleResult());

    }
}
