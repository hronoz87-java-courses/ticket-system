package org.example.app.repository;

import lombok.RequiredArgsConstructor;
import org.example.app.entity.CommentEntity;
import org.example.app.entity.TicketEntity;
import org.example.app.entity.UserEntity;
import org.example.framework.authentication.Authentication;
import org.example.framework.template.JpaTransactionTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class TicketRepository {
    private final JpaTransactionTemplate template;

    public TicketEntity createTicket(final Authentication authentication, final TicketEntity ticket) {
        return template.executeInTransaction(em -> {
            final String login = authentication.getLogin().toLowerCase();
            final UserEntity user = em.createQuery("SELECT e FROM UserEntity e WHERE e.login = :login", UserEntity.class)
                    .setParameter("login", login.toLowerCase()).getSingleResult();
            ticket.setUserId(user.getId());
            em.persist(ticket);
            return ticket;
        });
    }

    public List<TicketEntity> getAllTicketLogin(final Authentication authentication, final int limit, final int offset) {
        return template.executeInTransaction(em -> {
            final String login = authentication.getLogin().toLowerCase();
            final UserEntity user = em.createQuery("SELECT e FROM UserEntity e WHERE e.login = :login", UserEntity.class)
                    .setParameter("login", login.toLowerCase()).getSingleResult();
            final List<TicketEntity> tickets = em.createQuery("SELECT e FROM TicketEntity e WHERE e.userId = :userId", TicketEntity.class)
                    .setParameter("userId", user.getId())
                    .setFirstResult(offset)
                    .setMaxResults(limit)
                    .getResultList();
            return tickets;
        });
    }

    public TicketEntity getByTicketId(final Authentication authentication, final Long id) {
        return template.executeInTransaction(em -> {
            final String login = authentication.getLogin().toLowerCase();
            final UserEntity user = em.createQuery("SELECT e FROM UserEntity e WHERE e.login = :login", UserEntity.class)
                    .setParameter("login", login.toLowerCase()).getSingleResult();
            final TicketEntity ticketEntity = em.find(TicketEntity.class, id);
            em.createQuery("SELECT e FROM TicketEntity e WHERE e.userId = :id AND e.id = :ticketId", TicketEntity.class)
                    .setParameter("id", user.getId())
                    .setParameter("ticketId", ticketEntity.getId())
                    .getSingleResult();
            final List<CommentEntity> comments = em.createQuery("SELECT e FROM CommentEntity e WHERE e.ticketId = :ticketId", CommentEntity.class)
                    .setParameter("ticketId", ticketEntity.getId())
                    .getResultList();
            ticketEntity.setComments(String.valueOf(comments));
            return ticketEntity;
        });
    }

    public long getCloseTicket(final Authentication authentication, final Long id) {
        return template.executeInTransaction(em -> {
            final String login = authentication.getLogin().toLowerCase();
            final UserEntity user = em.createQuery("SELECT e FROM UserEntity e WHERE e.login = :login", UserEntity.class)
                    .setParameter("login", login.toLowerCase()).getSingleResult();
            final TicketEntity ticketEntity = em.find(TicketEntity.class, id);
            em.createQuery("SELECT e FROM TicketEntity e WHERE e.userId = :id AND e.id = :ticketId", TicketEntity.class)
                    .setParameter("id", user.getId())
                    .setParameter("ticketId", ticketEntity.getId())
                    .getSingleResult();
            ticketEntity.setStatus("CLOSE");
            em.merge(ticketEntity);
            return ticketEntity.getId();
        });
    }
}
