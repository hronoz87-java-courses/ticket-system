package org.example.app.repository;

import lombok.RequiredArgsConstructor;
import org.example.app.entity.CommentEntity;
import org.example.app.entity.TicketEntity;
import org.example.app.entity.UserEntity;
import org.example.framework.template.JpaTransactionTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@RequiredArgsConstructor
public class CommentRepository {
    private final JpaTransactionTemplate template;

    public UserEntity getByLogin(final String login) {
        return template.executeInTransaction(em ->
                em.createQuery("SELECT e FROM UserEntity e WHERE e.login = :login", UserEntity.class)
                        .setParameter("login", login.toLowerCase()).getSingleResult());

    }


    public List<TicketEntity> getAllTickets(final int limit, final int offset) {
        return template.executeInTransaction(em -> {
            return em.createQuery("SELECT e FROM TicketEntity e", TicketEntity.class)
                   .setFirstResult(offset)
                   .setMaxResults(limit)
                   .getResultList();
        });
    }

    public TicketEntity getByTicketId(final Long id) {
       return template.executeInTransaction(em -> {
            return em.find(TicketEntity.class, id);
        });
    }


    public CommentEntity createComment(final CommentEntity commentEntity, final Long id) {
        return template.executeInTransaction(em -> {
            final TicketEntity ticketEntity = em.find(TicketEntity.class, id);
            commentEntity.setTicketId(ticketEntity.getId());
            ticketEntity.setComments(String.valueOf(commentEntity));
            em.persist(commentEntity);
            em.persist(ticketEntity);
            return commentEntity;
        });
    }

    public List<CommentEntity> getCommentsByTicketId(final Long id) {
        return template.executeInTransaction(em -> {
           return em.createQuery("SELECT c FROM CommentEntity c WHERE c.ticketId = : id")
                    .setParameter("id", id)
                    .getResultList();
        });
    }
}
