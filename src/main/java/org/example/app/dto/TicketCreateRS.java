package org.example.app.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class TicketCreateRS {
    private Long id;
    private String name;
    private String content;
    private String comments;
    private String status;
}
