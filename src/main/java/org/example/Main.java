package org.example;

import org.apache.catalina.Context;
import org.apache.catalina.LifecycleException;
import org.apache.catalina.Wrapper;
import org.apache.catalina.connector.Connector;
import org.apache.coyote.http11.Http11NioProtocol;
import org.apache.coyote.http2.Http2Protocol;
import org.apache.tomcat.util.descriptor.web.ContextResource;
import org.apache.tomcat.util.descriptor.web.FilterDef;
import org.apache.tomcat.util.descriptor.web.FilterMap;
import org.apache.tomcat.util.net.SSLHostConfig;
import org.apache.tomcat.util.net.SSLHostConfigCertificate;

import org.example.framework.filter.BasicAuthenticationFilter;
import org.example.framework.filter.AnonymousAuthenticationFilter;
import org.example.framework.filter.X509AuthenticationFilter;
import org.example.framework.listener.ContextInitializationDestroyListener;
import org.example.framework.server.Server;
import org.example.framework.servlet.FrontServlet;

import javax.sql.DataSource;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;

public class Main {
  public static void main(String[] args) throws LifecycleException, IOException{
    final Server server = new Server();

    server.setPort(9999);

    final Http11NioProtocol protocol = new Http11NioProtocol();
    final Connector connector = new Connector(protocol);
    connector.setPort(9999);

    protocol.setMaxThreads(150);
    protocol.setSSLEnabled(true);
    connector.addUpgradeProtocol(new Http2Protocol());

    final SSLHostConfig sslHostConfig = new SSLHostConfig();
    sslHostConfig.setProtocols("TLSv1.3");
    sslHostConfig.setCertificateVerificationAsString("optional");
    sslHostConfig.setTruststoreFile("truststore.jks");
    sslHostConfig.setTruststorePassword("passphrase");
    connector.addSslHostConfig(sslHostConfig);

    final SSLHostConfigCertificate certificate = new SSLHostConfigCertificate(sslHostConfig, SSLHostConfigCertificate.Type.RSA);
    certificate.setCertificateKeystoreFile("server.jks");
    certificate.setCertificateKeystorePassword("passphrase");
    sslHostConfig.addCertificate(certificate);

    server.setConnector(connector);

    final Context context = server.createContext("",
        Files.createDirectories(Paths.get("static"))
            .toFile()
            .getAbsolutePath()
    );

    final ContextResource db = new ContextResource();
    db.setName("jdbc/db");
    db.setAuth("Container");
    db.setType(DataSource.class.getName());
    db.setProperty("url", Optional.ofNullable(System.getenv("JDBC_URL"))
            .orElse("jdbc:postgresql://localhost:5432/db?user=app&password=pass"));
    db.setProperty("maxTotal", "20");
    db.setProperty("maxIdle", "10");
    db.setCloseMethod("close");
    context.getNamingResources().addResource(db);

    context.addServletContainerInitializer(
        (c, ctx) -> {
          ctx.setInitParameter("basePackage", "org.example");
          ctx.addListener(new ContextInitializationDestroyListener());
        },
        null
    );

    final FilterDef filterDefAnon = new FilterDef();
    filterDefAnon.setFilter(new AnonymousAuthenticationFilter());
    filterDefAnon.setFilterName("anon");
    context.addFilterDef(filterDefAnon);

    final FilterMap filterMapAnon = new FilterMap();
    filterMapAnon.setFilterName(filterDefAnon.getFilterName());
    filterMapAnon.addURLPatternDecoded("/*");
    context.addFilterMap(filterMapAnon);

    final FilterDef filterDefX509 = new FilterDef();
    filterDefX509.setFilter(new X509AuthenticationFilter());
    filterDefX509.setFilterName("x509");
    context.addFilterDef(filterDefX509);

    final FilterMap filterMapX509 = new FilterMap();
    filterMapX509.setFilterName(filterDefX509.getFilterName());
    filterMapX509.addURLPatternDecoded("/*");
    context.addFilterMap(filterMapX509);

    final FilterDef filterDefBasic = new FilterDef();
    filterDefBasic.setFilter(new BasicAuthenticationFilter());
    filterDefBasic.setFilterName("basic");
    context.addFilterDef(filterDefBasic);

    final FilterMap filterMapBasic = new FilterMap();
    filterMapBasic.setFilterName(filterDefBasic.getFilterName());
    filterMapBasic.addURLPatternDecoded("/*");
    context.addFilterMap(filterMapBasic);


    // регистрация сервлета
    final Wrapper wrapper = context.createWrapper();
    wrapper.setServlet(new FrontServlet());
    wrapper.setName("front");
    wrapper.setLoadOnStartup(1);

    context.addChild(wrapper);
    context.addServletMappingDecoded("/", wrapper.getName());

    server.start();
  }
}
