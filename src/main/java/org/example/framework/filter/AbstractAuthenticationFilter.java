package org.example.framework.filter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpFilter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.framework.authentication.Authentication;
import org.example.framework.exception.AuthenticationException;

import java.io.IOException;

public abstract class AbstractAuthenticationFilter extends HttpFilter {
  @Override
  protected void doFilter(final HttpServletRequest req, final HttpServletResponse res, final FilterChain chain) throws IOException, ServletException {
    if (shouldNotApply(req)) {
      chain.doFilter(req, res);
      return;
    }

    try {
      doApply(req, res, chain);
    } catch (AuthenticationException e) {
      e.printStackTrace();
      res.sendError(401);
    } catch (Exception e) {
      e.printStackTrace();
      res.sendError(500);
    }
  }

  protected boolean shouldNotApply(final HttpServletRequest req) {
    final Authentication authentication = (Authentication) req.getAttribute(Authentication.ATTR_AUTH);

    if (authentication == null) {
      return false;
    }
    return !authentication.isAnonymous();
  }

  protected abstract void doApply(final HttpServletRequest req, final HttpServletResponse res, final FilterChain chain) throws Exception;
}
