package org.example.framework.filter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.framework.authentication.Authentication;
import org.example.framework.authentication.X509Authentication;
import org.example.framework.exception.CommonNameNotFoundException;

import java.io.IOException;
import java.security.cert.X509Certificate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class X509AuthenticationFilter extends AbstractAuthenticationFilter {
    public static final String X509_ATTR = "jakarta.servlet.request.X509Certificate";
    private final Pattern pattern = Pattern.compile("CN=(.*?)(?:,|$)", Pattern.CASE_INSENSITIVE); // from spring security

    @Override
    protected void doApply(final HttpServletRequest req, final HttpServletResponse res, final FilterChain chain)
            throws IOException, ServletException, CommonNameNotFoundException {
        final Object attribute = req.getAttribute(X509_ATTR);
        if (attribute == null) {
            chain.doFilter(req, res);
            return;
        }

        final X509Certificate[] certificates = (X509Certificate[]) attribute;
        final X509Certificate certificate = certificates[0];
        final String name = certificate.getSubjectX500Principal().getName();
        final Matcher matcher = pattern.matcher(name);
        if (!matcher.find()) {
            throw new CommonNameNotFoundException(name);
        }

        final String commonName = matcher.group(1);
        final Authentication authentication = new X509Authentication(commonName);
        req.setAttribute(Authentication.ATTR_AUTH, authentication);

        chain.doFilter(req, res);
    }
}
