package org.example.framework.filter;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.framework.authentication.AnonymousAuthentication;
import org.example.framework.authentication.Authentication;

import java.io.IOException;

public class AnonymousAuthenticationFilter extends AbstractAuthenticationFilter {
  @Override
  public boolean shouldNotApply(final HttpServletRequest req) {
    return req.getAttribute(Authentication.ATTR_AUTH) != null;
  }

  @Override
  protected void doApply(final HttpServletRequest req, final HttpServletResponse res, final FilterChain chain) throws IOException, ServletException {
    req.setAttribute(Authentication.ATTR_AUTH, new AnonymousAuthentication());
    chain.doFilter(req, res);
  }
}
