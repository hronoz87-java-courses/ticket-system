package org.example.framework.attribute;

public class ContextAttributes {
  public static final String ATTR_SPRINT_CONTEXT = "org.example.framework.attribute.context.SpringContext";
  private ContextAttributes() {
  }
}
