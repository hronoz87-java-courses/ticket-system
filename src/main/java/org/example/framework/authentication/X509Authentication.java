package org.example.framework.authentication;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class X509Authentication implements Authentication {
  private final String commonName;

  @Override
  public String getLogin() {
    return commonName;
  }

  @Override
  public boolean isAnonymous() {
    return false;
  }

  @Override
  public void eraseCredentials() {
  }
}
