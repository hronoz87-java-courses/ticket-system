package org.example.framework.listener;

import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import org.example.framework.attribute.ContextAttributes;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class ContextInitializationDestroyListener implements ServletContextListener {
    private ConfigurableApplicationContext springContext;

    @Override
    public void contextInitialized(final ServletContextEvent sce) {
        final String basePackage = sce.getServletContext().getInitParameter("basePackage");
        final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(basePackage);

        this.springContext = context;

        sce.getServletContext().setAttribute(
                ContextAttributes.ATTR_SPRINT_CONTEXT,
                springContext
        );
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        if (springContext != null) {
            springContext.close();
        }
    }

}
