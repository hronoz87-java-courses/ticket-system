package org.example.framework.servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.framework.attribute.ContextAttributes;
import org.example.framework.context.ContextBeanNames;
import org.example.framework.handler.Handler;
import org.springframework.context.ApplicationContext;

import java.io.IOException;
import java.util.Map;
import java.util.Optional;

public class FrontServlet extends HttpServlet {
  private final transient Handler notFoundHandler = (req, resp) -> resp.sendError(404);
  private final transient Handler internalServerErrorHandler = (req, resp) -> resp.sendError(500);
  private transient Map<String, Handler> routes;

  @Override
  public void init() throws ServletException {
    final ApplicationContext context = (ApplicationContext) getServletContext().getAttribute(ContextAttributes.ATTR_SPRINT_CONTEXT);
    routes = (Map<String, Handler>) context.getBean(ContextBeanNames.ROUTES);
  }

  @Override
  protected void service(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
    final String path = req.getRequestURI().substring(req.getContextPath().length());
    final Handler handler = Optional.ofNullable(routes.get(path))
        .orElse(notFoundHandler);
    try {
      handler.handle(req, resp);
    } catch (Exception e) {
      try {
        internalServerErrorHandler.handle(req, resp);
      } catch (Exception ex) {
        ex.printStackTrace();
      }
    }
  }
}
