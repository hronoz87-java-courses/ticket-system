INSERT INTO users(id, login, password, role, removed, created)
VALUES (0, 'sasha', 'support','SUPPORT', false, CURRENT_TIMESTAMP),
       (1, 'petya', 'secret', 'USER', false, CURRENT_TIMESTAMP),
       (2, 'vasya', 'secret', 'USER', false, CURRENT_TIMESTAMP);
